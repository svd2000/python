from sys import argv
text = argv[1:]
a = 'config_sw1.txt'
b = 'config_sw1_cleared.txt'
ignore = ['duplex', 'alias', 'Current configuration']
with open(a, 'r') as start, open(b, 'a') as end:
    for line in start:
        if line.find(ignore[0]) is -1 and line.find(ignore[1]) is -1 and line.find(ignore[2]) is -1:
            end.write(line)