from sys import argv
text = argv[1:]
text = 'config_sw1.txt'
ignore = ['duplex', 'alias', 'Current configuration']
with open(text, 'r') as file, open('config_sw1_cleared.txt', 'a') as f:
    for line in file:
        if line.find(ignore[0]) is -1 and line.find(ignore[1]) is -1 and line.find(ignore[2]) is -1:
            f.write(line)