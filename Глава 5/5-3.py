mode = input('Enter interface mode (access/trunk ):')
interface = input('Enter interface type and number:')
vlans = input ('Enter vlans(s):')

config_template = [
['switchport mode access',
 'switchport access vlan {}',
 'switchport nonegotiate',
 'spanning-tree portfast',
 'spanning-tree bpduguard enable'],
['switchport trunk encapsulation dot1q',
 'switchport mode trunk',
 'switchport trunk allowed vlan {}']
]

mode = mode.count('trunk')

print('\n' * 2)
print('interface {}'.format(interface))
print('\n'.join(config_template[mode]).format(vlans))